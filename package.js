Package.describe({
    name: 'molosc:popup',
    version: '0.0.18',
    // Brief, one-line summary of the package.
    summary: 'Popup for Meteor',
    // URL to the Git repository containing the source code for this package.
    git: 'https://Molosc@bitbucket.org/Molosc/popup.git',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.2.1');

    api.use('meteor-platform');

    api.use(['session', 'underscore']);
    api.use(['templating'], 'client');

    api.addFiles('popup.html', 'client');
    api.addFiles('popup.css', 'client');
    api.addFiles('popup.js', 'client');
    api.export('Popup', 'client');
});
/*
Package.onTest(function(api) {
    api.use('ecmascript');
    api.use('popup');
    api.use('molosc:popup');
    api.addFiles('popup-tests.js');
});
*/