/*!
 * Popup for Meteor
 *  
 * https://bitbucket.org/Molosc/popup
 *
 *
 */
if (Meteor.isClient) {

    Template.popup.created = function() {
        Session.set('popup', undefined);
    }
    Template.popup.rendered = function() {
        overlayColor: function() {
            return Template.instance().overlayColor.get();
        }
    }
    Template.popup.helpers({
        popup: function() {
            return Session.get('popup');
        }
    });
    Template.popup.events({
        'click .popup-overlay': function(e, t) {
            e.preventDefault();
            var popup = Session.get('popup');
            if (popup && popup.options.overlayClose) Popup.close();
            return false;
        },
        'click .popup-close': function(e, t) {
            e.preventDefault();
            Popup.close();
            return false;
        }
    });


    Popup = function() {

        var self = {};

        self.defaults = {
            // overlay
            overlayClass: '',
            overlayColor: 'rgba(0,0,0,0.8)',
            overlayClose: true,
            // content
            contentClass: '',
            contentWidth: '',
            // close btn
            showClose: true,
            closeLabel: 'x',
            closeIcon: null,
        }

        self.setDefaults = function(options) {
            self.defaults = _.extend(self.defaults, options);
        }

        self.open = function(template, data, options) {
            if (_.isUndefined(data)) data = {};
            if (_.isUndefined(options)) options = {};

            self.close();

            Session.set('popup', {
                template : template, 
                data : data, 
                options : _.extend(_.extend({}, self.defaults), options)
            });
            
        }

        self.close = function() {
            Session.set('popup', undefined);
        }
        self.overlayColor = function(color) {
            var p = Session.get('popup');
            if (p) {
                p.options.overlayColor = color;
            }
            Session.set('popup', p);
        }
        self.flashOverlayColor = function(color, time) {
            var p = Session.get('popup');
            if (p) {
                p.options.overlayColor = color;
            }
            Session.set('popup', p);
        }

        return self;

    }();

}
